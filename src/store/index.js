import Vue from 'vue'
import Vuex from 'vuex'
import Setting from './Setting/index'
import Category from './Category/index'
import Movie from './Movie/index'
import Show from './Show/index'
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'
let film = window.localStorage.getItem('filmFavorie');

export default new Vuex.Store({
  modules: {
    Setting,
    Category,
    Movie,
    Show
  },
  state: {
    film: film ? JSON.parse(film) : [],
  },
  mutations: {
    savefilm(state) {
      localStorage.setItem('filmFavorie', JSON.stringify(state.film))
    },
    _toggleFavorite(state, item) {
      let filmObject = {
        id: item.id,
        title: item.title,
        vote_average: item.vote_average,
        release_date: item.release_date,
        poster_path: item.poster_path
      }

      let indexOfExistingProduct = state.film.findIndex(
          (el) => el.id === filmObject.id
      );

      if(indexOfExistingProduct !== -1) {
        state.film[indexOfExistingProduct].qty += 1
      }
      else {
        state.film.push(filmObject);
      }

      this.commit('savefilm');
      console.log(state.film);
    }
  },
  actions: {
  },
  getters: {
  },
  strict: debug
})
